var webpack = require("webpack"),
  ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
  entry: {
    bootstrap: __dirname + "/app/scripts/bootstrap.js",
    home: __dirname + "/app/scripts/controllers/home.js",
    registration: __dirname + "/app/scripts/controllers/registration.js"
  },
  output: {
    path: "build",
    publicPath: "/",
    filename: "js/[name].js"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: (/node_modules/),
        query: {
          presets: ['es2015']
        }
      },
      {test: /\.jade$/, loader: "jade"},
      {test: /\.less$/, loader: ExtractTextPlugin.extract('style', 'css?minimize!less')},
      {test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css?minimize')},
      {test: /\.(png|jpg|gif|eot|svg|ttf|woff|woff2|otf)$/, loader: 'file-loader?name=css/fonts/[name].[ext]&limit=10000'}
    ]
  },
  resolve: {
    moduleDirectories: ['node_modules'],
    extensions: ['', '.js', '.json', '.png', '.gif', '.less', '.css', '.min.js']
  },
  resolveLoader: {
    moduleDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extentions: ['', '.js', '.jade']
  },
  plugins: [
    new ExtractTextPlugin("css/dist/[name].css")
  ],
  watch: NODE_ENV == 'development',
  watchOptions: {
    aggregateTimeout: 400
  },
  devtool: NODE_ENV == 'development' ? "source-map" : null
}


if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  )
}