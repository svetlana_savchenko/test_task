import express from "express";
import routers from "./app/routers/index.js";
var path = require('path');

let app = express();

// view's settings
app.set('views', path.join(__dirname, './app/views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'build')));
//app.use('/fonts', express.static(path.join(__dirname, '/css/fonts')));

// routers
app.use('/', routers);

// run server
app.listen(3000, () => console.log('Listening on port 3000...'));