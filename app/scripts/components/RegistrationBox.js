import Input from './Input.js';
import Observer from '../lib/Observer';

/**
 * Registration Box class
 */
class RegistrationBox extends Observer {
  /**
   * Constructor
   * @param {Node} elem
   */
  constructor(elem) {
    super();
    this.domElem = elem;
    this.form = {
      firstName: new Input(elem.querySelector('[data-name="firstName"]')),
      lastName: new Input(elem.querySelector('[data-name="lastName"]')),
      email: new Input(elem.querySelector('[data-name="email"]')),
      phone: new Input(elem.querySelector('[data-name="phone"]')),
      password: new Input(elem.querySelector('[data-name="password"]')),
      passwordConfirm: new Input(elem.querySelector('[data-name="passwordConfirm"]'))
    }

    this.addBtn = elem.querySelector('[data-action="add"]');

    this.initValidators();
    this.addBtn.addEventListener('click', () => this.submitForm() );
  }

  /**
   * Init validation rules
   */
  initValidators() {
    this.form.firstName.setValdators('required|isLength:min=3,max=20');
    this.form.lastName.setValdators('required|isLength:min=3,max=50');
    this.form.email.setValdators('required|isEmail');
    this.form.phone.setValdators('required|isPhone');
    this.form.password.setValdators('required|isLength:min=6,max=20');
    this.form.passwordConfirm.setValdators(
        "required|isLength:min=6,max=20|equal:val=" + ("[data-name='password'] input").replace('=', '%61'),
        {equal: 'Password and Confirm password are not equal'}
    );
  }

  /**
   * Submit form
   */
  submitForm() {
    let isFormValid = true;

    for(let inputBox in this.form) {
      this.form[inputBox].checkValidation();
      if (!this.form[inputBox].isInputValid) {isFormValid =  false;}
    }

    if (isFormValid) {
      console.log('ok');
      this.triggerEvent('signUp', {firstName: 'Vasya' });
    }
  }
}

export default RegistrationBox;