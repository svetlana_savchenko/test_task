import Observer from '../lib/Observer'
/**
 * Class MenuBox
 */
class MenuBox extends Observer {
  /**
   * Constructor
   */
  constructor(elem) {
    super();
    this.domElem = elem;
    this.alertSuccess = elem.querySelector('.alert-success');
    this.alertMsg = this.alertSuccess.querySelector('.alert-msg');
    this.logo = elem.querySelector('.logo');
    this.menu = elem.querySelector('ul');
  }

  signUp(msg) {
    this.showSuccessMsg(msg);

    setTimeout(() => {
      window.location.href = '/';
    }, 5000)
  }

  showSuccessMsg(msg) {
    this.alertMsg.innerHTML = msg;
    this.alertSuccess.classList.add('show-msg');
  }
}

export default MenuBox;