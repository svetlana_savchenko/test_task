import Validation from '../lib/Validation'

/**
 * Input class
 */
class Input extends Validation {
  /**
   * Constructor
   * @param {Node} elem
   */
  constructor(elem) {
    super();
    this.domElem = elem;
    this.rules = {};
    this.messages = {};
    this.isInputValid = true;
    this.currDate = new Date();
  }

  /**
   * Show error. Color input as error
   */
  showError() {
    this.domElem.classList.remove('has-success');
    this.domElem.classList.add('has-error');
    this.domElem.querySelector('.glyphicon').classList.remove('glyphicon-ok');
    this.domElem.querySelector('.glyphicon').classList.add('glyphicon-remove');
    this.domElem.querySelector('.err-msg').innerHTML = this.errorMessage;
  }

  /**
   * Color input as success
   */
  showSuccess() {
    this.domElem.classList.remove('has-error');
    this.domElem.classList.add('has-success');
    this.domElem.querySelector('.glyphicon').classList.remove('glyphicon-remove');
    this.domElem.querySelector('.glyphicon').classList.add('glyphicon-ok');
    this.domElem.querySelector('.err-msg').innerHTML = '';
  }

  /**
   * Clear error
   */
  clearError() {
    this.errorMessage = '';
  }

  /**
   * Validation of input
   */
  checkValidation() {
    let val = this.domElem.querySelector('input').value.trim();

    this.isInputValid = this.isValid(this.rules, val);

    if (!this.isInputValid) {
      this.showError()
    } else {
      this.showSuccess()
    };
  }

  /**
   * Set Validators to element
   * @param {String} rules
   * @param {Object} messages
   * @param {Boolean} autovalidate
   */
  setValdators(rules, messages = {}, autovalidate = true) {
    this.rules = rules;
    this.messages = messages

    if (autovalidate) {
      this.domElem.addEventListener(
        "input",
        (e) => {
          let event = e;
          setTimeout(() => {
            let tLastCall = this.getDiffSec();

            if (tLastCall < 2) {
              event.stopImmediatePropagation();
              return;
            } else {
              this.currDate = new Date();
              this.checkValidation()
            }
          }, 2000);
        }
      );
    }
  }

  /**
   * Difference between time.now and previous change input
   * @returns {number}
   */
  getDiffSec() {
    let today = new Date();
    return (today.getTime() - this.currDate.getTime()) / 1000;
  }

  /**
   * Get input value
   * @returns {HTMLElement.value|*}
   */
  getValue() {
    return this.domElement.querySelector('input').value
  }
}

export default Input;