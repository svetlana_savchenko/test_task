import Observer from '../lib/Observer';
/**
 * ListBox class
 */
class ListBox extends Observer {
  /**
   * Constructor
   * @param {Node} elem
   */
  constructor(elem) {
    super();
    this.domElement = elem;
    this.input = elem.querySelector('input')
    this.btnAdd = elem.querySelector('[data-action="add"]')
    this.btnRemove = elem.querySelector('[data-action="remove"]')

    this.init();
  }

  /**
   * init
   */
  init() {
    this.checkLockAddBtn();
    this.checkLockRemoveBtn();

    this.btnAdd.addEventListener('click', () => this.addBtnClick());
    this.btnRemove.addEventListener('click', () => this.triggerEvent('btnDeleteClick'));
    this.input.addEventListener('input', () => this.checkLockAddBtn());
  }

  /**
   * lock / unlock remove button
   * @param {Node} item
   */
  checkLockRemoveBtn(item = false) {
    item && item.hasAttribute('selected')
      ? this.btnRemove.removeAttribute('disabled')
      : this.btnRemove.setAttribute('disabled', '');
  }

  /**
   * lock / unlock add button
   */
  checkLockAddBtn() {
    this.input.value.trim() === ''
      ? this.btnAdd.setAttribute('disabled', '')
      : this.btnAdd.removeAttribute('disabled');
  };

  /**
   * Add element
   */
  addBtnClick() {
    if (this.input.value === '') { alert('Please enter the name of artist'); return; }

    this.triggerEvent('add', this.input.value);
    this.input.value = '';
    this.checkLockAddBtn();
  }

  /**
   * Delete element
   */
  deleteBtnClick(id) {
    if (!id) {return;}

    let t = confirm("Selected element will be deleted. Are you sure ?");
    if (t) {
      this.triggerEvent('delete', id);
      this.checkLockRemoveBtn();
    }
  }
}

export default ListBox;