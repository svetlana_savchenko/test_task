import Observer from '../lib/Observer';
/**
 * List class
 */
class List extends Observer {
  /**
   * Constructor
   * @param {Node} elem
   * @param {Object} options
   */
  constructor(elem, options) {
    super();
    this.store = options.store;
    this.rightArrow = options.rightArrow || false;
    this.domElement = elem;
    this.ul = document.createElement('ul');

    this.create();

    this.ul.addEventListener(
      'click',
      (event)=> {
        if(event.target.tagName.toLowerCase() == 'li') {
          this.selectElement(event.target.getAttribute('data-id'));
        }
      })

    this.store.addListener(
      'query',
      (items) => {
        this.ul.innerHTML = "";
        items.forEach((item) => this.addElement(item))
      })
  }

  /**
   * Create list and append it to parentElement
   */
  create() {
    this.store
      .query(() => true)
      .forEach((item)=> this.addElement(item));
    this.domElement.appendChild(this.ul);
  }

  /**
   * Add element
   * @param {id: String, name: String} obj
   */
  addElement(obj) {
    let li = document.createElement('li');

    li.setAttribute('data-id', obj._id)
    li.innerHTML = obj.name;

    if (this.rightArrow) {
      let icon = document.createElement('i');
      icon.setAttribute('class', 'glyphicon glyphicon-chevron-right pull-right');

      li.appendChild(icon);
    }

    this.ul.appendChild(li);
  }

  /**
   * HighLight element
   */
  selectElement(id) {
    let li = this.getLiById(id);
    debugger;
    this.isSelected(li)
      ? li.removeAttribute('selected')
      : li.setAttribute('selected', true);

    this.triggerEvent('selected', this.store.byId(id));
  }

  /**
   * Remove selected element
   */
  removeSelectedElement() {
    let li = this.getSelectedElement();
    this.remove(li);
  }

  /**
   * Get selected item
   * @return {Node}
   */
  getSelectedElement() {
    return this.domElement.querySelector('li[selected]');
  }

  /**
   * Get <li> element by id
   * @param {Number} id
   */
  getLiById(id) {
    return this.ul.querySelector('[data-id="' + id + '"]');
  }

  /**
   * Remove element
   * @param {Number} id
   */
  remove(object) {
    let el = this.getLiById(object._id);

    el.parentNode.removeChild(el);
  }

  /**
   * Check if element selected
   * @param {Node} el
   * @returns {boolean}
   */
  isSelected(el) {
    return !!el.hasAttribute('selected');
  }

  /**
   * Unselect elements
   */
  unSelectElements(el) {
    [].forEach.call(
      this.domElement.querySelectorAll('li[selected]'),
      (item) => {
        if(item.getAttribute('data-id') != el._id) {
          item.removeAttribute('selected')
        }
      }
    );
  }
}

export default List;