import ObjectStore from '../lib/ObjectStore';
import RegistrationBox from '../components/RegistrationBox';
import MenuBox from '../components/MenuBox';

let
  // box selectors
  boxFormRegistration = document.querySelector(".form-login"),
  boxMenu = document.querySelector(".navbar-header"),

  // work with store
  users = new ObjectStore({data: []}),

  // work with DOM
  registrationBox = new RegistrationBox(boxFormRegistration),
  menuBox = new MenuBox(boxMenu);

registrationBox.addListener('signUp', (user) => menuBox.signUp('User has been registered.') )
