import List from '../components/List';
import ListBox from '../components/ListBox';
import ObjectStore from '../lib/ObjectStore';

let arrArtists = [
    {name: 'John Lennon', genre: 'Rock'},
    {name: 'Gary Moore', genre: 'Rock'},
    {name:'Steve Vai', genre: 'Rock'},
    {name: 'Ronnie James Dio', genre: 'Rock'},
    {name: 'Brithney Spears', genre: 'Pop'},
    {name: 'lady gaga', genre: 'Pop'}
  ],
  arrGenres = [
    {name: 'Rock'},
    {name: 'Pop'},
    {name: 'Jazz'},
    {name: 'Bluz'}
  ],
  artistsBox = document.getElementById('artists-box'),
  genresBox = document.getElementById('genres-list'),
  addArtistsBox = document.querySelector('#add-artist-box'),
  // work with store
  genres = new ObjectStore({data: arrGenres}),
  artists = new ObjectStore({data: arrArtists}),
  // work with DOM
  genresList = new List(genresBox, {store: genres, rightArrow: true}),
  artistsList = new List(artistsBox, {store: artists}),
  addArtistsList = new ListBox(addArtistsBox);


//add artist's listeners
artists.addListener('add', (obj) => artistsList.addElement(obj));
artists.addListener('delete', (obj) => artistsList.remove(obj));

artistsList.addListener('selected', (item) => {
  artistsList.unSelectElements(item);
  addArtistsList.triggerEvent('selected', artistsList.getLiById(item._id));
});

// add genre's listeners
genresList.addListener('selected', (item) => {
  genresList.unSelectElements(item);
  artists.query( (artist) => artist.genre == item.name );
  addArtistsList.triggerEvent('selected', artistsList.getLiById(item._id));
});

// addArtistsList's listeners
addArtistsList.addListener('selected', (el) => addArtistsList.checkLockRemoveBtn(el));
addArtistsList.addListener('add', (val) => {
  let genre = genresList.getSelectedElement() ? genresList.getSelectedElement().innerHTML : '';
  artists.add({genre: genre, name: val })
});
addArtistsList.addListener('delete', (id) => artists.delete(id) );
addArtistsList.addListener('btnDeleteClick', () => {
  let el = artistsList.getSelectedElement();
  addArtistsList.deleteBtnClick(el.getAttribute('data-id'))
})

//console.dir(artists);
