/**
 * Observer class
 */
class Observer {
  /**
   * Constructor
   */
  constructor() {
    this.listeners = {}
  }

  /**
   * Add listener to object listeners
   * @param {String} evt
   * @param {Callback} callback
   * @returns undefined
   */
  addListener(evt, callback) {
    if ( !this.listeners.hasOwnProperty(evt) ) {
      this.listeners[evt] = [];
    }

    this.listeners[evt].push(callback);
  }

  /**
   * Remove listener from object listeners
   * @param {String} evt
   * @param {Callback} callback
   * @returns undefined
   */
  removeListener(evt, callback) {
    if ( this.listeners.hasOwnProperty(evt) ) {
      for (let i = 0, length = this.listeners[evt].length; i < length; i += 1) {
        if( this.listeners[evt][i] === callback) {
          this.listeners[evt].splice(i, 1);
        }
      }
    }
  }

  /**
   * Emit event from listener's object
   * @param {String} evt
   * @param {Object} args
   * @returns undefined
   */
  triggerEvent(evt, args) {
    if ( this.listeners.hasOwnProperty(evt) )    {
      for (let i = 0, length = this.listeners[evt].length; i < length; i += 1) {
        this.listeners[evt][i](args);
      }
    }
  }

  getListeners() {
    return this.listeners
  }
}

export default Observer;