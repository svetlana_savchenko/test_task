import Observer from './Observer';
let _counter = 0;

/**
 * Increment for identifier of store
 * @returns {number}
 */
function increment() {
  return _counter++;
}

/**
 * Class for store list
 */
class ObjectStore extends Observer {
  /**
   * Constructor
   * @param {Object} object
   */
  constructor(object) {
    super();
    this.items = [];
    this.mapById = {};
    this.init(object.data);
  }

  /**
   * Fill store by initial data
   * @param {Array} arr
   * @returns undefined
   */
  init(arr = []) {
    arr.forEach((item) => {
      this.add(item);
    })
  }

  /**
   * Add object to store
   * @param {Object} object
   * @returns undefined
   */
  add(object) {
    if (!object._id) {
      object._id = increment();
    }
    if(this.mapById[object._id]) {
      throw new Error("Such index already exists")
    }

    this.items.push(object);
    this.mapById[object._id] = object;

    this.triggerEvent('add', object);
  }

  byId(id) {
    return this.mapById[id];
  }

  /**
   * Delete object from store
   * @param {Number} id
   * @returns undefined
   */
  delete(id) {
    let item = this.mapById[id];
    let itemIdx = this.items.indexOf(item);
    this.items.splice(itemIdx, 1);
    delete this.mapById[id];
    this.triggerEvent('delete', item);
  }

  query(queryFunc) {
    let result = this.items.filter(queryFunc);
    this.triggerEvent('query', result);
    return result;
  }


}

export default ObjectStore;