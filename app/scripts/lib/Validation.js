/**
 * Validation
 */
class Validation {
  /**
   * Constructor
   */
  constructor() {
    this.errorMessage = '';
    this.flValid = true;
    this.defaultOptions = {
      min: 3,
      max: 250
    };
    this.regExpr = {
      isEmail: /[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/i,
      isPhone: /^\s*(?:\+?(\d{1,3}))?([- (]*(\d{3})[- )]*)?((\d{3})[- ]*(\d{2,4})(?:[-x ]*(\d+))?)\s*$/i
    }
  }

  /**
   * Check if value accordance to rules
   * @param {String} rules
   * @param {String} val
   * @returns {boolean}
   */
  isValid(rules, val) {
    this.flValid = true;
    this.errorMessage = '';

    rules.split('|').forEach((rule) => {
      if (this.errorMessage != '') return;

      let objRule = rule.split(':'),
          nameRule = objRule[0],
          strParams = objRule[1];

      switch (nameRule) {
        case 'required': this.checkRequired(nameRule, val); break;
        case 'isLength': this.checkLength(nameRule, val, strParams); break;
        case 'isEmail':
        case 'isPhone': this.checkRegExpr(nameRule, val); break;
        case 'equal': this.checkEqual(nameRule, val, strParams); break;
      }
    })

    return this.flValid;
  }

  /**
   * Validation for Require
   * @param {String} nameRule
   * @param {String} val
   * @returns undefined
   */
  checkRequired(nameRule, val) {
    if (val === '') {
      this.flValid = false;
      this.errorMessage = this.messages[nameRule] || 'Field is required';
    }
  }

  /**
   * Validation for Length
   * @param {String} nameRule
   * @param {String} val
   * @param {String} strParams
   * @returns undefined
   */
  checkLength(nameRule, val, strParams) {
    let params = this.parseParams(strParams),
      min = params['min'] || this.defaultOptions.min,
      max = params['max'] || this.defaultOptions.max;

    if ((params['min'] && val.length < params['min'])
      || (params['max'] && val.length > params['max'])) {
      this.flValid = false;

      this.errorMessage = (this.messages[nameRule] || 'Value must be between :min and :max chars long')
        .replace(':min', min)
        .replace(':max', max);
    }
  }

  /**
   * Validation for RegExpr
   * @param {String} nameRule
   * @param {String} val
   * @returns undefined
   */
  checkRegExpr(nameRule, val) {
    let res = val.match(this.regExpr[nameRule]);

    if (!res || res[0] !== val) {
      this.flValid = false;

      if (this.messages[nameRule]) {
        this.errorMessage = this.messages[nameRule];
      } else {
        switch(nameRule) {
          case 'isEmail': this.errorMessage = 'Invalid email'; break;
          case 'isPhone': this.errorMessage = 'Invalid phone number. Can contain digits, spaces, brackets, signs: plus or minus'; break;
        }
      }
    }
  }

  /**
   * Validation for Equal
   * @param {String} nameRule
   * @param {String} val
   * @param {String} strParams
   * @returns undefined
   */
  checkEqual(nameRule, val, strParams) {
    let params = this.parseParams(strParams);
    let valParams = document.querySelector(params.val.replace('%61', '=')).value;

    if (val !== valParams) {
      this.flValid = false;
      this.errorMessage = this.messages[nameRule];
    }
  }

  /**
   * Parse parameters
   * @param {String} strParams
   * @returns {Object}
   */
  parseParams(strParams) {
    let res = {},
      arrParams = strParams.split(',');

    arrParams.forEach((param) => {
      let arr = param.split('='),
          name = arr[0] || false,
          val = arr[1] || false;

      if (name && val) {
        res[name] = val;
      }
    })
    return res;
  }
}

export default Validation;