import express from "express";

let router = express.Router();

/**
 * GET home page.
 */
router.get('/', (req, res) => res.render('home', { title: 'Artists'}) );

/**
 * GET login page.
 */
router.get('/signin', (req, res) => res.render('login', { title: 'Artists | SignIn'}) );

/**
 * GET registration page.
 */
router.get('/signup', (req, res) => res.render('registration', { title: 'Artists | SignUp'}) );

module.exports = router;